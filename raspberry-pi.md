## Raspberry Pi

###### Create compressed image
  `sudo dd bs=4M if=/dev/sdc |gzip > /path/to/image_$(date +%F).gz`

###### Restore compressed image
  `gzip -dc /path/to/image.gz | sudo dd bs=4M of=/dev/sdb`

###### Shrink image
  Download: https://github.com/Drewsif/PiShrink
  `sudo pishrink.sh imagefile.img`

###### Force HDMI every boot
  Edit `/boot/config.txt`:
  Add these lines:
    `#Always force HDMI output and enable HDMI sound`
    `hdmi_force_hotplug=1`
    `hdmi_drive=2`

###### Fix date and time:
  `Create symbolic link: ln -s /usr/share/zoneinfo/America/Detroit /etc/localtime`

###### Creating a new Retropie image
  * Edit /boot/config.txt
    Uncomment:
      hdmi_force_hotplug=1
      hdmi_drive=2
  * Set wi-fi country
  * Connect to wi-fi
  * Set locale
  * Set keyboard layout
  * install git, ntp, vim-nox, tmux
  * Set timezone
  * Add ssh key to authorized_hosts
  * install script: wget -O - "https://raw.githubusercontent.com/RetroFlag/retroflag-picase/master/install.sh" | sudo bash
  * Change megadrive branding to genesis, create /opt/retropie/configs/all/platforms.cfg:
      megadrive_theme="genesis"
      megadrive_platform="genesis"
  * Update main packages from binary
  * Copy initial roms
  * Copy initial saves
  * Configure sega genesis controller
  * Update save file location:
      Edit: /opt/retropie/configs/all/retroarch.cfg:
        savefile_directory = /home/pi/RetroPie/saves/srm
        savestate_directory = /home/pi/RetroPie/saves/state
  * Add syncthing binary to /home/pi/.config/syncthing
  * Add syncthing config file
  * Add syncthing startup file to ~/.config/systemd/user/syncthing.service
  * add ssh key
  * git clone git@gitlab.com:jsherman82/retropie_configs.git
  * simlink bios
  * simlink /opt/retropie/configs/all/emulationstation/gamelists to ~/git/retropie_configs/gamelists
  * simlink /opt/retropie/configs/all/emulationstation/downloaded_images to ~/git/retropie_configs/downloaded_images
